phpunit-object-enumerator (7.0.0-1) experimental; urgency=medium

  * Upload new major to experimental

  [ Sebastian Bergmann ]
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * Revert "Force system dependencies loading"
  * Simplify build
  * Update copyright (years)
  * Update debian/clean

 -- David Prévot <taffit@debian.org>  Sat, 08 Feb 2025 12:48:58 +0100

phpunit-object-enumerator (6.0.1-3) unstable; urgency=medium

  * Source-only upload for testing migration

 -- David Prévot <taffit@debian.org>  Fri, 10 Jan 2025 12:06:12 +0100

phpunit-object-enumerator (6.0.1-2) unstable; urgency=medium

  * Upload to unstable in sync with PHPUnit 11

 -- David Prévot <taffit@debian.org>  Fri, 10 Jan 2025 09:20:49 +0100

phpunit-object-enumerator (6.0.1-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Use PHPStan instead of Psalm
  * Prepare release

  [ David Prévot ]
  * Update Standards-Version to 4.7.0

 -- David Prévot <taffit@debian.org>  Sun, 14 Jul 2024 11:37:26 +0200

phpunit-object-enumerator (6.0.0-2) experimental; urgency=medium

  * Force system dependencies loading

 -- David Prévot <taffit@debian.org>  Tue, 05 Mar 2024 20:07:48 +0100

phpunit-object-enumerator (6.0.0-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Start development of next major version
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * Update copyright (years)

 -- David Prévot <taffit@debian.org>  Sat, 03 Feb 2024 14:19:55 +0100

phpunit-object-enumerator (5.0.0-1) experimental; urgency=medium

  * Upload new major version to experimental

  [ Sebastian Bergmann ]
  * Drop support for PHP 8.0
  * Bump copyright year
  * Add Security Policy
  * Prepare release

  [ David Prévot ]
  * Drop now useless overrides
  * Update standards version to 4.6.2, no changes needed.
  * Update copyright (years and license)
  * Ship upstream security notice
  * Use phpunit 10 for tests

 -- David Prévot <taffit@debian.org>  Sun, 05 Feb 2023 11:12:47 +0100

phpunit-object-enumerator (4.0.4-2) unstable; urgency=medium

  * Simplify gbp import-orig (and check signature)
  * Install and use phpabtpl(1) autoloaders
  * Use dh-sequence-phpcomposer instead of --with phpcomposer
  * Mark package as Multi-Arch: foreign
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Fri, 08 Jul 2022 06:10:05 +0200

phpunit-object-enumerator (4.0.4-1) unstable; urgency=medium

  * Upload to unstable in sync with PHPUnit 9

  [ Sebastian Bergmann ]
  * Prepare release

  [ David Prévot ]
  * Update watch file format version to 4.
  * Update Standards-Version to 4.5.1

 -- David Prévot <taffit@debian.org>  Sun, 20 Dec 2020 15:02:51 -0400

phpunit-object-enumerator (4.0.3-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Prepare release

  [ David Prévot ]
  * Rename main branch to debian/latest (DEP-14)
  * Set Rules-Requires-Root: no.

 -- David Prévot <taffit@debian.org>  Wed, 30 Sep 2020 13:45:02 -0400

phpunit-object-enumerator (4.0.2-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Ignore tests etc. from archive exports
  * Support PHP 8 for https://github.com/sebastianbergmann/phpunit/issues/4325
  * Prepare release

  [ David Prévot ]
  * Document gbp import-ref usage
  * Use debhelper-compat 13
  * Simplify override_dh_auto_test
  * Drop patch applied upstream

 -- David Prévot <taffit@debian.org>  Mon, 29 Jun 2020 11:17:01 -1000

phpunit-object-enumerator (4.0.0-1) experimental; urgency=medium

  * Upload version compatible with PHPUnit 9 to experimental

  [ Sebastian Bergmann ]
  * Bump copyright year
  * Drop support for PHP 7.0, PHP 7.1, and PHP 7.2
  * Prepare release

  [ David Prévot ]
  * debian/copyright: Update copyright (years)
  * debian/upstream/metadata:
    set fields Bug-Database, Bug-Submit, Repository, Repository-Browse
  * debian/control:
    + Drop versioned dependency satisfied in (old)stable
    + Update standards version to 4.5.0

 -- David Prévot <taffit@debian.org>  Fri, 07 Feb 2020 22:36:25 -1000

phpunit-object-enumerator (3.0.3-4) unstable; urgency=medium

  * Drop get-orig-source target
  * Use versioned copyright format URI.
  * Bump debhelper from old 9 to 12.
  * Compatibility with recent PHPUnit (8)
  * Simplify testsuite handling

 -- David Prévot <taffit@debian.org>  Sat, 24 Aug 2019 10:47:09 -1000

phpunit-object-enumerator (3.0.3-3) unstable; urgency=medium

  * Add missing dependency for ci (Closes: #889113)
  * Move project repository to salsa.d.o
  * Update Standards-Version to 4.1.5

 -- David Prévot <taffit@debian.org>  Wed, 04 Jul 2018 07:57:50 -1000

phpunit-object-enumerator (3.0.3-2) unstable; urgency=medium

  * Upload to unstable, with the rest of the latest PHPUnit stack
  * Update Standards-Version to 4.1.1

 -- David Prévot <taffit@debian.org>  Sat, 25 Nov 2017 16:00:37 -1000

phpunit-object-enumerator (3.0.3-1) experimental; urgency=medium

  * Upload version only compatible with latest PHPUnit to experimental

  [ Sebastian Bergmann ]
  * Depend on sebastian/object-reflector
  * Prepare release

  [ David Prévot ]
  * Adapt autoload to new phpunit-object-reflector dependency
  * Don’t use default upstream build system
  * Update copyright (years)
  * Update Standards-Version to 4.1.0
  * Adapt test calls to latest upstream updates

 -- David Prévot <taffit@debian.org>  Tue, 22 Aug 2017 21:16:24 -1000

phpunit-object-enumerator (1.0.0-2) unstable; urgency=medium

  * Rebuild with recent pkg-php-tools for the PHP 7.0 transition

 -- David Prévot <taffit@debian.org>  Wed, 09 Mar 2016 18:14:33 -0400

phpunit-object-enumerator (1.0.0-1) unstable; urgency=low

  * Initial release (new phpunit dependency)

 -- David Prévot <taffit@debian.org>  Sun, 07 Feb 2016 13:55:36 -0400
